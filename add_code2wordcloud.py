# -*- coding: utf-8 -*-
"""
Created on Sat May 25 12:15:17 2024

@author: benjamin_arrondeau

@description: This script can be use if you want to add a new code to the code.txt file.
        The way to add a code in that list is to put it at the very end of the list.
        Indeed, this script will compare an arbitrary noisy wordcloud with a fixed positions one 
        (defined by the x_rand.npy and y_rand.npy files of this repo) and add only the 
        extra last points to those files.
"""

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import random
import matplotlib.font_manager as font_manager

font_path = '/usr/share/fonts/truetype/msttcorefonts/Impact.ttf'
font_manager.fontManager.addfont(font_path)
prop = font_manager.FontProperties(fname=font_path)

plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = prop.get_name()

# On personal computer
# cwd = "/home/benjamin_arrondeau/Workspace/Diamond/infographie_diamond"
# With Docker
cwd = "/work"
# For CI/CD
# cwd = "."

######################
#### UTILS
######################

def sort_words_by_x(code_list_immobile, code_list, x_rand, y):
    
    for code in code_list_immobile:
        # no shift in x
        x_rand[np.where(code_list==code)[0][0]]=0
        # no other code on this y
        y_forbidden = y[np.where(code_list==code)][0]
        x_code = scale_list[np.where(code_list==code)][0]
        pos_problem = np.where(pos==float(x_code))[0][0]
        if pos_problem==0:
            x2change = [pos_problem+1]
        elif pos_problem==len(pos)-1:
            x2change = [pos_problem-1]
        else:
            x2change = [pos_problem-1,pos_problem+1]
        # This tricky one ...............
        if code=="quantum-espresso":
            x2change=[pos_problem-1,pos_problem+1,pos_problem+2,pos_problem+3]
        for tmp_x in x2change:
            x = pos[tmp_x]
            ind2check = np.where(scale_list.astype(float)==x)
            y_pos2check = y[ind2check]
            if y_forbidden in y_pos2check:
                n=np.random.randint(y_min,3,1)/6.5
                while n in y_pos2check:
                    n=np.random.randint(y_min,3,1)/6.5
                ind2replace=ind2check[0][np.where(y_pos2check==y_forbidden)[0][0]]
                y[ind2replace]=n


def sort_by_scale(code_list_immobile, code_list, scale_list, pos):
    
    new_array = np.empty(len(code_list_immobile), dtype='<U16')
    ind_array = []
    
    for code in code_list_immobile:
        ind_array.append(np.where(code_list==code)[0][0])
    
    n=0
    for x in pos:
        for i in ind_array:
            if float(scale_list[i])==x:
                new_array[n]=code_list[i]
                n+=1
        
    return new_array


def check_empty_y_pos(y, pos):
    
    # On exclut la position pour les micro-structures ...
    
    
    for x in pos:
        ind = np.where(scale_list.astype(float)==x)[0]
        available_y_pos=np.linspace(y_min,3,abs(y_min)+3+1)/6.5
        used_y = []
        for j in ind:
            used_y.append(y[j])
        for y_coord in available_y_pos[1:][::-1]:
            if y_coord not in used_y:
                # print(y_coord)
                for j in ind:
                    if y[j]<y_coord:
                        y[j]+=abs(available_y_pos[1]-available_y_pos[0])


######################
#### INIT DATA
######################

code_path = cwd+"/codes.txt"

code_list = np.loadtxt(code_path,dtype=str,usecols=0,skiprows=1)
iteration_list = np.loadtxt(code_path,dtype=int,usecols=1,skiprows=1)
scale_list = np.loadtxt(code_path,dtype=str,usecols=2,skiprows=1)

code_list_immobile = ["quantum-espresso", "ovito", "lammps", "abinit", \
                      "vasp", "vesta", "vmd", "atomeye", "modena", \
                      "paraview", "abaqus", "xcrysden"]    
    
scale_list[scale_list=='none']='999'
it = np.array(range(len(iteration_list)))
random.shuffle(it)
y_min=-30
n_code=len(code_list)

n_code_visual=len(np.where(scale_list!='999')[0])

######################
#### PRE PROCESS
######################

##### For the wordcloud

## x position
tmp_x = [(np.random.sample(1)/5-0.2) for i in range(n_code)]
x_rand=np.zeros((n_code))
for i in range(n_code):
    x_rand[i]=tmp_x[i][0]

x_fixed=np.load(cwd+'/x_rand.npy')

## y position
y_rand = np.zeros((n_code))
tmp_pos = np.unique(scale_list)
pos = tmp_pos.astype(float)
pos = np.delete( pos, np.where(pos==999) )
pos = np.sort(pos)
for x in pos:
    ind = np.where(scale_list.astype(float)==x)[0]
    random_pos = np.random.choice(range(y_min,3), len(ind), replace=False)/6.5
    for i in range(len(ind)):
        y_rand[ind[i]]=random_pos[i]

code_list_immobile = sort_by_scale(code_list_immobile, code_list, scale_list, pos)
        
sort_words_by_x(code_list_immobile, code_list, x_rand, y_rand)
sort_words_by_x(code_list_immobile, code_list, x_rand, y_rand)
sort_words_by_x(code_list_immobile, code_list, x_rand, y_rand)
check_empty_y_pos(y_rand, pos)
    
y_fixed=np.load(cwd+'/y_rand.npy')
delta_y=1/6.5
    
##### now we compare the sizes of the arrays
n_diff = len(x_rand) - len(x_fixed)
if n_diff>0:
    # only case where a code was added
    n_previous = len(x_fixed)
    # we add the element one by one
    for i in range(n_diff):
        x_fixed = np.append(x_fixed, [x_rand[n_previous+i]])
        y_fixed = np.append(y_fixed, [y_rand[n_previous+i]])


    np.save(cwd+'/x_test.npy', x_fixed)
    np.save(cwd+'/y_test.npy', y_fixed)
    print("Codes added successfully!")