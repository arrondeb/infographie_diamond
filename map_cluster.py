#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 10:52:16 2024

@author: benjamin_arrondeau
"""

# import the necessary packages
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import geopandas as gpd
import pandas as pd
import numpy as np
import matplotlib.font_manager as font_manager
import sys

font_path = '/usr/share/fonts/truetype/msttcorefonts/Impact.ttf'
font_manager.fontManager.addfont(font_path)
prop = font_manager.FontProperties(fname=font_path)

plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = prop.get_name()

show = False
if len(sys.argv) > 1 :
    if sys.argv[1] == show : show = True


# On personal computer
# cwd = "/home/benjamin_arrondeau/Workspace/Diamond/infographie_diamond"
# With Docker
# cwd = "/work"
# For CI/CD
cwd = "."

######################
#### UTILS
######################

def plot_radial_bar(ax, percentage, color):
    
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(1)
    ax.set_yticklabels([])

    # Create coloured ring
    ax.barh(4, 2*np.pi, color='gray', alpha=0.25)
    ax.barh(4, percentage*2*np.pi/100, color=color)

    # Hide all grid elements for the    
    ax.grid(False)
    ax.tick_params(axis='both', left=False, bottom=False, 
                       labelbottom=False, labelleft=True)

######################
#### INIT DATA
######################
# load the low resolution world map
world = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres"))

# grab the country of Kenya
continent = world[world["continent"] == "Europe"]
france = continent[continent["name"] == "France"]

df = pd.DataFrame(
    {
        "City": ["Nantes", "Lyon", "Grenoble", "Rennes", "Nancy", "Lille", "Nancy", "Sophia", "Clermont", "Chambéry", "Besançon", "Orléans", "Tours", "Strasbourg", "Metz", "Reims", "Amiens", "Rouen", "Bordeaux", "Toulouse", "Montpellier", "Angers", "Le Mans", "Marseille", "Paris", "Cergy", "Palaiseau", "Sorbonne", "Meudon", "Saclay", "Nice", "Bruyères-le-Châtel", "Orsay"],
        "Latitude": [47.22, 45.76, 45.19, 48.11, 48.69, 50.64, 48.69, 43.62, 45.77, 45.57, 47.24, 47.90, 47.39, 48.58, 49.12, 49.26, 49.89, 49.44, 44.84, 43.6, 43.61, 47.47, 48, 43.3, 48.85, 49.05, 48.71, 48.85, 48.81, 48.73, 43.70, 48.59, 48.70],
        "Longitude": [-1.55, 4.83, 5.74, -1.68, 6.18, 3.06, 6.18, 7.05, 3.08, 5.92, 6.02, 1.91, 0.69, 7.75, 6.18, 4.03, 2.30, 1.09, -0.58, 1.44, 3.88, -0.55, 0.20, 5.37, 2.35, 2.04, 2.25, 2.35, 2.24, 2.17, 7.27, 2.19, 2.19],
    }
)

list_container = ["Grenoble", "Lyon", "Rennes", "Nancy", "Lille", "Nantes", "Sophia", \
                  "Chambéry", "Besançon", "Strasbourg" , "Angers", "Le Mans", "Montpellier", \
                  "Bruyères-le-Châtel", "Orsay"]

list_oar = ["Grenoble", "Rennes", "Nancy", "Lille", "Nantes", "Sophia"]
    
list_no_slurm = ["Grenoble", "Rennes", "Nancy", "Lille", "Nantes", "Sophia", \
                 "Chambéry", "Besançon", "Cergy", "Sorbonne", "Bruyères-le-Châtel"]

container_sol_df = df[df['City'].isin(list_container)]
no_container_sol_df = df[~df['City'].isin(list_container)]
no_slurm_sol_df = df[df['City'].isin(list_no_slurm)]
slurm_sol_df = df[~df['City'].isin(list_no_slurm)]
oar_sol_df = df[df['City'].isin(list_oar)]
    
container_sol_gdf = gpd.GeoDataFrame(
    container_sol_df, geometry=gpd.points_from_xy(container_sol_df.Longitude, container_sol_df.Latitude), crs="EPSG:4326"
)

no_container_sol_gdf = gpd.GeoDataFrame(
    no_container_sol_df, geometry=gpd.points_from_xy(no_container_sol_df.Longitude, no_container_sol_df.Latitude), crs="EPSG:4326"
)
    
slurm_sol_df = gpd.GeoDataFrame(
    slurm_sol_df, geometry=gpd.points_from_xy(slurm_sol_df.Longitude, slurm_sol_df.Latitude), crs="EPSG:4326"
)

no_slurm_sol_df = gpd.GeoDataFrame(
    no_slurm_sol_df, geometry=gpd.points_from_xy(no_slurm_sol_df.Longitude, no_slurm_sol_df.Latitude), crs="EPSG:4326"
)

oar_sol_df = gpd.GeoDataFrame(
    oar_sol_df, geometry=gpd.points_from_xy(oar_sol_df.Longitude, oar_sol_df.Latitude), crs="EPSG:4326"
)


##
code_path = cwd+"/clusters.txt"

cluser_list = np.loadtxt(code_path,dtype=str,usecols=0,skiprows=1)
scheduler_list = np.loadtxt(code_path,dtype=str,usecols=1,skiprows=1)
container_list = np.loadtxt(code_path,dtype=str,usecols=2,skiprows=1)
guix_list = np.loadtxt(code_path,dtype=str,usecols=3,skiprows=1)

n_container = len(np.where(container_list=='yes')[0])
n_guix = len(np.where(guix_list=='yes')[0])
n_total_container= len(np.where(container_list!='undefined')[0])
n_total_guix = len(np.where(guix_list!='undefined')[0])

n_slurm = len(np.where(scheduler_list=='SLURM')[0])
n_oar = len(np.where(scheduler_list=='OAR')[0])
n_total_scheduler = len(np.where(scheduler_list!='undefined')[0])

######################
#### PLOT EVERYTHING (LIGHT MODE) FR
######################

####### CONTENEURISE ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="black")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="k", linewidth=5, zorder=1)

container_sol_gdf.plot(ax=ax, color="darkred", markersize=300)
no_container_sol_gdf.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("ont un système\nde conteneurs", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction')
# plt.annotate("have a container\nsystem", xy=(1.225, .8), xytext=(1.225, .8), \
#                 size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_container/n_total_container)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_container/n_total_container)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]

plt.annotate("ont guix", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction')
# plt.annotate("have guix", xy=(1.05, -1), xytext=(1.05, -1), \
#                 size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_guix/n_total_guix)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_guix/n_total_guix)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/FR/cluster_conteneurise_light.png', bbox_inches='tight', transparent="True")
if show : plt.show()

####### SCHEDULER ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="black")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="k", linewidth=5, zorder=1)

slurm_sol_df.plot(ax=ax, color="darkred", markersize=300)
no_slurm_sol_df.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)
oar_sol_df.plot(ax=ax, color="darkgreen", markersize=300)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("utilise le\nscheduler SLURM", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction')
# plt.annotate("use the\nSLURM scheduler", xy=(1.225, .8), xytext=(1.225, .8), \
#                 size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_slurm/n_total_scheduler)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_slurm/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]
plt.annotate("utilise le\nscheduler OAR", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction')
# plt.annotate("use the\nOAR scheduler", xy=(1.05, -1), xytext=(1.05, -1), \
#                 size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_oar/n_total_scheduler)*100,1), 'darkgreen')
ax.text(x=0, y=0, s=str(np.round((n_oar/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/FR/cluster_scheduler_light.png', bbox_inches='tight', transparent="True")
if show : plt.show()

######################
#### PLOT EVERYTHING (DARK MODE) FR
######################

####### CONTENEURISE ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="white")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="white", linewidth=5, zorder=1)

container_sol_gdf.plot(ax=ax, color="darkred", markersize=300)
no_container_sol_gdf.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("ont un système\nde conteneurs", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
# plt.annotate("have a container\nsystem", xy=(1.225, .8), xytext=(1.225, .8), \
#                 size=35, ha="left", va="center", xycoords='axes fraction', \
#                 color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_container/n_total_container)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_container/n_total_container)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]
plt.annotate("ont guix", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
# plt.annotate("have guix", xy=(1.05, -1), xytext=(1.05, -1), \
#                 size=35, ha="left", va="center", xycoords='axes fraction', \
#                 color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_guix/n_total_guix)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_guix/n_total_guix)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/FR/cluster_conteneurise_dark.png', bbox_inches='tight', transparent="True")
if show : plt.show()

####### SCHEDULER ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="white")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="white", linewidth=5, zorder=1)

slurm_sol_df.plot(ax=ax, color="darkred", markersize=300)
no_slurm_sol_df.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)
oar_sol_df.plot(ax=ax, color="darkgreen", markersize=300)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("utilise le\nscheduler SLURM", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
# plt.annotate("use the\nSLURM scheduler", xy=(1.225, .8), xytext=(1.225, .8), \
#                 size=35, ha="left", va="center", xycoords='axes fraction', \
#                 color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_slurm/n_total_scheduler)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_slurm/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]
plt.annotate("utilise le\nscheduler OAR", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
# plt.annotate("use the\nOAR scheduler", xy=(1.05, -1), xytext=(1.05, -1), \
#                 size=35, ha="left", va="center", xycoords='axes fraction', \
#                 color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_oar/n_total_scheduler)*100,1), 'darkgreen')
ax.text(x=0, y=0, s=str(np.round((n_oar/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/FR/cluster_scheduler_dark.png', bbox_inches='tight', transparent="True")
if show : plt.show()


######################
#### PLOT EVERYTHING (LIGHT MODE) EN
######################

####### CONTENEURISE ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="black")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="k", linewidth=5, zorder=1)

container_sol_gdf.plot(ax=ax, color="darkred", markersize=300)
no_container_sol_gdf.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("have a container\nsystem", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_container/n_total_container)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_container/n_total_container)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]

plt.annotate("have guix", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_guix/n_total_guix)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_guix/n_total_guix)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/EN/cluster_conteneurise_light.png', bbox_inches='tight', transparent="True")
if show : plt.show()

####### SCHEDULER ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="black")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="k", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="k", linewidth=5, zorder=1)

slurm_sol_df.plot(ax=ax, color="darkred", markersize=300)
no_slurm_sol_df.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)
oar_sol_df.plot(ax=ax, color="darkgreen", markersize=300)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="black")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("use the\nSLURM scheduler", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_slurm/n_total_scheduler)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_slurm/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]
plt.annotate("use the\nOAR scheduler", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction')
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_oar/n_total_scheduler)*100,1), 'darkgreen')
ax.text(x=0, y=0, s=str(np.round((n_oar/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/EN/cluster_scheduler_light.png', bbox_inches='tight', transparent="True")
if show : plt.show()

######################
#### PLOT EVERYTHING (DARK MODE) FR
######################

####### CONTENEURISE ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="white")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="white", linewidth=5, zorder=1)

container_sol_gdf.plot(ax=ax, color="darkred", markersize=300)
no_container_sol_gdf.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("have a container\nsystem", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_container/n_total_container)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_container/n_total_container)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]
plt.annotate("have guix", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_guix/n_total_guix)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_guix/n_total_guix)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/EN/cluster_conteneurise_dark.png', bbox_inches='tight', transparent="True")
if show : plt.show()

####### SCHEDULER ??
fig = plt.figure(figsize=(20, 10), dpi=200)

rect = [0,0,0.5,1]
ax = fig.add_axes(rect, frameon=False, clip_box=None)

france.clip_by_rect(-6,41,11,52).plot(ax=ax, color="lightgray", edgecolor="white")

init_point_CINES = container_sol_gdf[container_sol_gdf["City"]=="Montpellier"].geometry
init_point_CEA = container_sol_gdf[container_sol_gdf["City"]=="Bruyères-le-Châtel"].geometry
init_point_IDRIS = container_sol_gdf[container_sol_gdf["City"]=="Orsay"].geometry

ax.plot([float(init_point_CINES.x),-2], [float(init_point_CINES.y),42], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_CEA.x),-3], [float(init_point_CEA.y),46], color="white", linewidth=5, zorder=1)
ax.plot([float(init_point_IDRIS.x),-2], [float(init_point_IDRIS.y),51], color="white", linewidth=5, zorder=1)

slurm_sol_df.plot(ax=ax, color="darkred", markersize=300)
no_slurm_sol_df.plot(ax=ax, color="darkred", markersize=300, alpha=0.25)
oar_sol_df.plot(ax=ax, color="darkgreen", markersize=300)

ax.text(x=-2.5, y=41.5, s="CINES", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-3.5, y=45.5, s="TGCC/CCRT", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.text(x=-2.5, y=51.5, s="IDRIS", \
        size=40, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.7,0.2,0.2]
plt.annotate("use the\nSLURM scheduler", xy=(1.225, .8), xytext=(1.225, .8), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_slurm/n_total_scheduler)*100,1), 'darkred')
ax.text(x=0, y=0, s=str(np.round((n_slurm/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")
    
## and, indicator
rect = [0.45,0.4,0.2,0.2]
plt.annotate("use the\nOAR scheduler", xy=(1.05, -1), xytext=(1.05, -1), \
                size=35, ha="left", va="center", xycoords='axes fraction', \
                color="white")
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_oar/n_total_scheduler)*100,1), 'darkgreen')
ax.text(x=0, y=0, s=str(np.round((n_oar/n_total_scheduler)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center', \
        color="white")
ax.axis("off")

# set the plot title
plt.axis("off")
plt.savefig(cwd+'/PNG/EN/cluster_scheduler_dark.png', bbox_inches='tight', transparent="True")
if show : plt.show()


