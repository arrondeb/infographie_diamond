# Infographie pour le projet DIAMOND

Le but de ce dépôt est de fournir les scripts python pour générer un nuage de mots des codes demandés par la communauté et un carte de la France des mesoscentres et centre de calcul nationaux, tous deux avec des métriques spécifiques.

## Ajouter des entrées au nuages de codes

Si vous voulez ajouter des codes à l'infographie, voici les étapes à suivre :

- dans un premier temps, vous devez ajouter les codes au fichier `codes.txt`. **Attention**, les nouveaux codes doivent être ajoutés à la fin de liste pour le bon fonctionnement des scripts.
- dans un second temps, vous devez exécuter le script `add_code2wordcloud.py` en local sur votre machine. Cela produira deux nouveaux tableaux de positions aléatoires `x_test.npy` et `y_test.npy`. Vous pouvez tester ces positions avec le script `test_random.py` qui générera un nuage de code via ces fichiers.
- une fois que vous êtes satisfait des positions, vous pouvez renommer les fichiers `*_test.npy` en `*_fixed.npy`, puis commit tous les changements sur le dépôt 