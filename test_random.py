# -*- coding: utf-8 -*-
"""
Created on Sat May 25 12:15:17 2024

@author: benjamin_arrondeau
"""

import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
import random
import matplotlib.font_manager as font_manager

font_path = '/usr/share/fonts/truetype/msttcorefonts/Impact.ttf'
font_manager.fontManager.addfont(font_path)
prop = font_manager.FontProperties(fname=font_path)

plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = prop.get_name()

# On personal computer
# cwd = "/home/benjamin_arrondeau/Workspace/Diamond/infographie_diamond"
# With Docker
# cwd = "/work"
# For CI/CD
cwd = "."

######################
#### UTILS
######################

def couleur(case: int):
    
    if case==1:
        return (round(random.randint(75, 150)/255,2), 0, 0.1)
    elif case==2:
        return (0.2, round(random.randint(0, 150)/255,2), 0.8)
    elif case==3:
        return (0.4, 0.59, round(random.randint(0, 150)/255,2))
    elif case==4:
        return (0.95, 0.65, round(random.randint(0, 150)/255,2))
    elif case==99:
        return (0.1, 0.1, 0.1, round(random.randint(25, 75)/255,2))
    elif case==100:
        return (0.9, 0.9, 0.9, round(random.randint(25, 75)/255,2))
    elif case==101:
        return (round(random.randint(175, 250)/255,2), 0, 0.1)


def plot_radial_bar(ax, percentage, color):
    
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(1)
    ax.set_yticklabels([])

    # Create coloured ring
    ax.barh(4, 2*np.pi, color='gray', alpha=0.25)
    ax.barh(4, percentage*2*np.pi/100, color=color)

    # Hide all grid elements for the    
    ax.grid(False)
    ax.tick_params(axis='both', left=False, bottom=False, 
                       labelbottom=False, labelleft=True)


def find_and_fix_pos(code, y, delta_y, integer):
    i=np.where(code_list==code)[0][0]
    y[i]=y[i]+integer*delta_y

######################
#### INIT DATA
######################

code_path = cwd+"/codes.txt"

code_list = np.loadtxt(code_path,dtype=str,usecols=0,skiprows=1)
iteration_list = np.loadtxt(code_path,dtype=int,usecols=1,skiprows=1)
scale_list = np.loadtxt(code_path,dtype=str,usecols=2,skiprows=1)
scale1_list = np.loadtxt(code_path,dtype=str,usecols=3,skiprows=1)
scale2_list = np.loadtxt(code_path,dtype=str,usecols=4,skiprows=1)
utility_list = np.loadtxt(code_path,dtype=str,usecols=5,skiprows=1)
guix_list = np.loadtxt(code_path,dtype=str,usecols=6,skiprows=1)
apptainer_list = np.loadtxt(code_path,dtype=str,usecols=7,skiprows=1)
prop_licence_list = np.loadtxt(code_path,dtype=str,usecols=8,skiprows=1)
ML_DL_list = np.loadtxt(code_path,dtype=str,usecols=9,skiprows=1)
aiida_list = np.loadtxt(code_path,dtype=str,usecols=10,skiprows=1)
doc_list = np.loadtxt(code_path,dtype=str,usecols=11,skiprows=1)

code_list_immobile = ["quantum-espresso", "ovito", "lammps", "abinit", \
                      "vasp", "vesta", "vmd", "atomeye", "modena", \
                      "paraview", "abaqus", "xcrysden"]    
    
scale_list[scale_list=='none']='999'
it = np.array(range(len(iteration_list)))
random.shuffle(it)
y_min=-30
n_code=len(code_list)

axis_ticks = np.array([0,1,2,3,4])
# axis_labels = ["electronic", "atomic", "mesoscopic", "microstructure", "macroscopic"]
axis_labels = ["électronique", "atomique", "mésoscopique", "microstructure", "macroscopique"]

n_code_visual=len(np.where(scale_list!='999')[0])

######################
#### PRE PROCESS
######################

##### For the wordcloud
x_rand=np.load(cwd+'/x_fixed.npy')

y=np.load(cwd+'/y_fixed.npy')
delta_y=1/6.5
    
# Fix epw pos
find_and_fix_pos("epw", y, delta_y, 2)
# Fix atomsk pos
find_and_fix_pos("atomsk", y, delta_y, 1)
# Fix plumed pos
find_and_fix_pos("plumed", y, delta_y, 2)
# Fix lammps pos
find_and_fix_pos("lammps", y, delta_y, 2)
# Fix raspa2 pos
find_and_fix_pos("raspa2", y, delta_y, -1)
# Fix merope pos
find_and_fix_pos("merope", y, delta_y, 2)
# Fix vmd pos
find_and_fix_pos("vmd", y, delta_y, 1)
# Fix zeo++ pos
find_and_fix_pos("zeo++", y, delta_y, -4)
# Fix mfront pos
find_and_fix_pos("mfront", y, delta_y, -1)
# Fix ansys pos
find_and_fix_pos("ansys", y, delta_y, 2)
# Fix visit pos
find_and_fix_pos("visit", y, delta_y, 1)
# Fix abinit pos
find_and_fix_pos("abinit", y, delta_y, 2)
# Fix quantum-espresso pos
find_and_fix_pos("quantum-espresso", y, delta_y, -1)
# Fix dftb+ pos
find_and_fix_pos("dftb+", y, delta_y, 3)
# Fix vaspkit pos
find_and_fix_pos("vaspkit", y, delta_y, 3.25)
# Fix jmol pos
find_and_fix_pos("jmol", y, delta_y, 3)
# Fix siesta pos
find_and_fix_pos("siesta", y, delta_y, -0.7)
# Fix xcrysden pos
find_and_fix_pos("xcrysden", y, delta_y, -1)
# Fix crystal pos
find_and_fix_pos("crystal", y, delta_y, 14.75)
# Fix crystal-maker pos
find_and_fix_pos("crystal-maker", y, delta_y, -0.75)
# Fix atomsk pos
find_and_fix_pos("atomsk", y, delta_y, -0.25)

### Huge shifts
# Fix craft pos
find_and_fix_pos("craft", y, delta_y, 4)
# Fix kineclue pos
find_and_fix_pos("kineclue", y, delta_y, 4)

# Fix granoo pos
find_and_fix_pos("granoo", y, delta_y, 1)
# Fix cast3m pos
find_and_fix_pos("cast3m", y, delta_y, 8)
# Fix neper pos
find_and_fix_pos("neper", y, delta_y, 9)

# Fix freefem++ pos
find_and_fix_pos("freefem++", y, delta_y, -1)
# Fix fenics pos
find_and_fix_pos("fenics", y, delta_y, 4)
# Fix ansys pos
find_and_fix_pos("ansys", y, delta_y, 4)
# Fix gmsh pos
find_and_fix_pos("gmsh", y, delta_y, 4)
# Fix paraview pos
find_and_fix_pos("paraview", y, delta_y, 4)
# Fix opencalphad pos
find_and_fix_pos("opencalphad", y, delta_y, 4)
# Fix abaqus pos
find_and_fix_pos("abaqus", y, delta_y, 5)
# Fix thermo-calc pos
find_and_fix_pos("thermo-calc", y, delta_y, 4)
# Fix pycalphad pos
find_and_fix_pos("pycalphad", y, delta_y, 4)
# Fix comsol pos
find_and_fix_pos("comsol", y, delta_y, 4)
# Fix visit pos
find_and_fix_pos("visit", y, delta_y, 4)
    
cmap_conteurise = np.empty((n_code), dtype=object)
cmap_conteurise_dark = np.empty((n_code), dtype=object)
cmap_licence = np.empty((n_code), dtype=object)
cmap_MLDL = np.empty((n_code), dtype=object)
cmap_utilite = np.empty((n_code), dtype=object)
cmap_aiida = np.empty((n_code), dtype=object)
cmap_doc = np.empty((n_code), dtype=object)

for i in range(n_code):
    
    # Utilité ?
    cmap_utilite[i]=couleur(2)
    
    # Conteneurise ? (Light mode)
    if guix_list[i]=='yes' or apptainer_list[i]=='yes':
        cmap_conteurise[i]=couleur(1)
    else:
        cmap_conteurise[i]=couleur(99)
    
    # Conteneurise ? (Dark mode)
    if guix_list[i]=='yes' or apptainer_list[i]=='yes':
        cmap_conteurise_dark[i]=couleur(101)
    else:
        cmap_conteurise_dark[i]=couleur(100)
            
    # Licence ?
    if prop_licence_list[i]=='yes':
        cmap_licence[i]=couleur(99)
    else:
        cmap_licence[i]=couleur(2)
            
    # Machine learning ?
    if ML_DL_list[i]=='yes':
        cmap_MLDL[i]=couleur(3)
    else:
        cmap_MLDL[i]=couleur(99)
            
    # Aiida ?
    if aiida_list[i]=='yes':
        cmap_aiida[i]=couleur(3)
    else:
        cmap_aiida[i]=couleur(99)

    # Documenté ?
    if doc_list[i]=='no':
        cmap_doc[i]=couleur(99)
    elif doc_list[i]=="both":
        cmap_doc[i]=couleur(4)
    else :
        cmap_doc[i]=couleur(4)

######################
#### PLOT EVERYTHING (LIGHT MODE)
######################

####### UTILITE ??
fig = plt.figure( figsize=(20,10), dpi=100 )
plt.xlim([-0.25,axis_ticks[-1]+0.8])
plt.ylim([-2.5,0.9])

## First, wordcloud
plt.annotate("", xy=(0-0.2, 0.75), xytext=(axis_ticks[-1]+0.5, 0.75), \
              arrowprops=dict(arrowstyle="<-", color='black', lw=8, mutation_scale=50))
for j in range(len(axis_ticks)):
    plt.annotate(axis_labels[j], xy=(axis_ticks[j], 0.7), xytext=(axis_ticks[j], 0.85), \
                 ha="center", va="bottom", font="Impact", size=30, \
                 arrowprops=dict(arrowstyle="-", color='black', lw=4))
for i in it:
    if scale_list[i]!='999':
        plt.text(x=float(scale_list[i])/10*axis_ticks[-1]+x_rand[i], y=y[i]+0.2, s=code_list[i], \
                size=int(1.2*iteration_list[i]+18), \
                horizontalalignment='center', verticalalignment='center', \
                font='Impact', color=cmap_utilite[i])

plt.axis("off")

## and, indicator
rect = [0.325,0.1,0.2,0.2]
plt.annotate("de codes\npour le\ncalcul", xy=(2.07, -2.15), xytext=(2.07, -2.15), \
            size=35, ha="left", va="center")
plt.annotate("d'outils de\nvisualisation", xy=(3.7, -2.15), xytext=(3.7, -2.15), \
            size=35, ha="left", va="center")
n_computation = len(np.where(utility_list[np.where(scale_list!='999')]=='computation')[0])
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_computation/n_code_visual)*100,1), (0.2, 0.25, 0.8))
ax.text(x=0, y=0, s=str(np.round((n_computation/n_code_visual)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")


rect = [0.575,0.1,0.2,0.2]
n_visualization = len(np.where(utility_list[np.where(scale_list!='999')]=='visualization')[0])
ax = fig.add_axes(rect, polar=True, frameon=False, clip_box=None)
plot_radial_bar(ax, np.round((n_visualization/n_code_visual)*100,1), (0.1, 0, 0.9))
ax.text(x=0, y=0, s=str(np.round((n_visualization/n_code_visual)*100,1))+"%", \
        size=45, horizontalalignment='center', verticalalignment='center')
ax.axis("off")

# save everything finally
plt.savefig(cwd+'/PNG/test_random.png', bbox_inches='tight')
plt.show()
